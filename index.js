// console.log("hello world");
// let nickname = "Kenneth"; 

function printInput(){
	// let nickname = prompt("Enter your nickname: ");
	//console.log("hi," +nickname);
}
printInput();

// Consider this function
    //parameter passing


                   //parameter
    function printName(name) {
    	console.log("My name is " + name);

    }
      //argument
    printName("Kenneth");
    printName("Louie");
    printName();

  //[SECTION] parameters and Arguments 

     //parameter
        //"name" is called a parameter
        // acts as a named variable/container that only exists a function.
        // it is used to store information that is provided to a function
        // when it is called /invoked.

        // Arguments
          // "Kenneth" is the value/data passed directly into the 
             //function,

             //value passed when invoking a function are called
             // arguments.
             //those arguments are then stored as the paramets within the function.

     function checkDivisibilityBy8(num){
        let remainder = num % 8;
        console.log("The remainder of " + num + " divided by 8 is: " + remainder);
        let isDivisibleBy8 = remainder === 0;
        console.log("Is " + num + " divisible by 8?");
        console.log(isDivisibleBy8);
    }

    checkDivisibilityBy8(10);
    

    // A variable as the argument
    let num1 = 64;
    checkDivisibilityBy8(num1);

    // [SECTION] Function as Arguments

        // Function parameters can also accepts other function as
        //arguments.
        // Some complex function use other functions as arguments to perform more
        // completed results.
        //

        function argumentFunc() {
        	console.log("This function was passed as an arugument before the messgae was printed");

        }
        function invokeFunc(argFunc){
        	console.log(argFunc);
        	argFunc()
        }
        // Adding and removing the parenthesis "()" impacts the output
        //of javascript.
        //function is used with parenthesis it denotes invoke/call
        //function is used without a parenthesis is normally 
        // associated with using a function as an argument to another
        //function.
        invokeFunc(argumentFunc);

    // [SECTION] Multiple Parameters
        //Multiple "arguments" will correspond to the number of
        // "parameters" declared in a function in succeeding order.

        function createFullName(firstName,lastName,middleName){
        	console.log(firstName+" "+middleName+" "+lastName);
        }

        createFullName("Juan", "Enye", "Dela Cruz");
        createFullName("Juan", "Dela Cruz");
        createFullName("Juan", "Enye", "Dela Cruz", "JR");

        // Using variables as arguments

        let firstName = "John";
        let middleName ="Doe";
        let lastName = "Smith";

        createFullName(firstName, middleName, lastName);

        // Note : The order of the argument is the same to the order of
        // the parameters. the first argument will be stored in the first
        // parameters, second argument wiill be stored in the second
        // parameter and so on.

        //[SECTION] Return Statement
           // allows us to output a value from a function to be passed to the line
            // the line/code block of the code that invoked/called the
            //fucntion.
            // we can further use/ manipulate in our program instead of
            //only printing/display

            function returnFullName(firstName, middleName, lastName){
            	//return Jeffrey Smith Doe
            	return firstName + " " + middleName + " " +lastName;
            	//return indicates the end of function execution
            	//it will ignore any codes after return statements
            	console.log(firstName+" "+middleName+" "+lastName);
            }

            let completeName = returnFullName("Jeffrey", "Smith", "Doe")
            console.log(completeName);

            console.log("My completeName is " +completeName);


     function checkDivisibilityBy5(num){
        let rem = remainder(num);
        console.log("The remainder of " + num + " divided by 5 is: " + rem);
        let isDivisibleBy8 = rem === 0;
        console.log("Is " + num + " divisible by 5?");
        console.log(isDivisibleBy8);
    }

    function remainder(num){
    	return num % 5;
    }
    checkDivisibilityBy5(21);

      // You can also create a variable inside the function to contain the result
      // and return the variable.

      function returnAddress(city, country){
      	let fullAddress = city + ", "+ country;
      	return fullAddress;
      }

      let myAddress = returnAddress("Cebu City", "Cebu");
      console.log(myAddress);

      // when a function only has a console.log() to display the
      // result it will return undefined instead.

      function printPlayerInfo(username, level, job){
      	console.log("Username: " +username);
      	console.log("level " +level);
      	console.log("Job :" +job);
      }

      let user = printPlayerInfo("knight_white", 95, "paladin");
      // You cannot save any value form printPlayerInfo() because
      // it does not return anything.
      console.log(user);